#!/bin/sh
export MONO_IOMAP=all
VSToolsPath=msbuild
Configuration=Release
DeployTo=UNIX.SERVER

set -e

msbuild msbuild/build.proj /flp:LogFile=Build.log
msbuild msbuild/deploy.proj /flp:LogFile=Deploy.log /p:DeployTo=$DeployTo

cd install/deb
rm -R -f ../*.deb ../*.changes Files/Services Files/WebStudio Files/sql Files/licenses
mkdir Files/sql
cp -Rv ../../../licenses Files/licenses
cp -Rv ../../deploy/$DeployTo/Services Files
cp -Rv ../../deploy/$DeployTo/WebStudio Files/WebStudio
cp -v ../../sql/onlyoffice* Files/sql

if [ -t 0 ]; then
	# if in terminal
	dpkg-buildpackage -b
else
	dpkg-buildpackage -b -p"$SIGN_COMMAND"
fi
mv -f ../onlyoffice* ../../deploy/