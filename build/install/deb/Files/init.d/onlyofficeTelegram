#!/bin/sh
### BEGIN INIT INFO
# Provides: onlyofficeTelegram
# Required-Start:	$syslog $local_fs $remote_fs $network 
# Required-Stop:	$syslog $local_fs $remote_fs $network 
# Default-Start:	2 3 4 5
# Default-Stop:		0 1 6
# Short-Description: ONLYOFFICE Telegram Service
### END INIT INFO

. /lib/lsb/init-functions
. /etc/teamlabsvc.env

# Define other required variables
SERVICE_NAME="Telegram"
NAME="onlyoffice$SERVICE_NAME"
DESC="ONLYOFFICE $SERVICE_NAME service"
USER="onlyoffice"
BOOT_LOG_FILE="/var/log/boot_$NAME.log"
PID_FILE="/tmp/$NAME.pid"
DAEMON=/usr/bin/mono-service
DAEMON_OPTS="-d:/var/www/onlyoffice/Services/TeamLabSvc -l:/tmp/onlyofficeTelegram /var/www/onlyoffice/Services/TeamLabSvc/TeamLabSvc.exe --service \"ASC.TelegramService.Launcher,ASC.TelegramService\" --log Telegram"
PRE_START_CMD="rm -f /tmp/$NAME"

if [ ! -x "$DAEMON" ]; then
	echo "The $NAME startup script does not exists or it is not executable, tried: $DAEMON"
	exit 1
fi

case "$1" in
  start)

	log_daemon_msg "Starting $DESC"

	pid=`pidofproc -p $PID_FILE $NAME`
	if [ -n "$pid" ] ; then
		log_begin_msg "Already running."
		log_end_msg 0
		exit 0
	fi
	
	#pre start command
	eval $PRE_START_CMD > $BOOT_LOG_FILE 2>&1 
	if [ $? -ne 0 ]; then
		log_failure_msg "Unable to bootstrap $DESC"
		exit 1
	fi

	# Start Daemon
    log_daemon_msg "Starting $DESC"
	start-stop-daemon --start --user $USER -c $USER --pidfile "$PID_FILE" --exec $DAEMON -- $DAEMON_OPTS >> $BOOT_LOG_FILE 2>&1
	return=$?
    log_daemon_msg "Waiting for $DESC to finish startup ..."
	if [ $return -eq 0 ]; then
		i=0
		timeout=10
		# Wait for the process to be properly started before exiting
		until { kill -0 `cat "$PID_FILE"`; } >/dev/null 2>&1
		do
			sleep 1
        
    log_daemon_msg "Waiting for $DESC to finish startup ..."
			i=$(($i + 1))
			if [ $i -gt $timeout ]; then
				log_end_msg 1
				exit 1
			fi
		done
	fi
	log_end_msg $return
	exit $return
	;;
  stop)
	log_daemon_msg "Stopping $DESC"

	if [ -f "$PID_FILE" ]; then
		start-stop-daemon --stop --pidfile "$PID_FILE" \
			--user $USER \
			--quiet \
			--retry forever/TERM/20 > /dev/null
		if [ $? -eq 1 ]; then
			log_progress_msg "$DESC is not running but pid file exists, cleaning up"
		elif [ $? -eq 3 ]; then
			PID="`cat $PID_FILE`"
			log_failure_msg "Failed to stop $DESC (pid $PID)"
			exit 1
		fi
		rm -f "$PID_FILE"
	else
		log_progress_msg "(not running)"
	fi
	log_end_msg 0
	;;
  status)
	status_of_proc -p $PID_FILE $DAEMON $NAME && exit 0 || exit $?
	;;
  restart|force-reload)
	if [ -f "$PID_FILE" ]; then
		$0 stop
	fi
	$0 start
	;;
  *)
	log_success_msg "Usage: $0 {start|stop|restart|force-reload|status}"
	exit 1
	;;
esac

exit 0